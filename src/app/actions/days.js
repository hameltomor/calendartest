//Fetch days list
export const GET_DAYS_REQUEST = 'FETCH_DAYS_REQUEST';
export const GET_DAYS_SUCCESS = 'GET_DAYS_SUCCESS';

//Fetch day
export const GET_DAY_REQUEST = 'GET_DAY_REQUEST';
export const GET_DAY_SUCCESS = 'GET_DAY_SUCCESS';

//Create new day
export const CREATE_DAY_REQUEST  = 'CREATE_DAY_REQUEST';
export const CREATE_DAY_SUCCESS = 'CREATE_DAY_SUCCESS';

//Update day
export const UPDATE_DAY_REQUEST = 'UPDATE_DAY_REQUEST';
export const UPDATE_DAY_SUCCESS  = 'UPDATE_DAY_SUCCESS';

//Delete day
export const DELETE_DAY_REQUEST = 'DELETE_DAY_REQUEST';
export const DELETE_DAY_SUCCESS = 'DELETE_DAY_SUCCESS';

//Get task
export const GET_TASK_REQUEST = 'GET_TASK_REQUEST';
export const GET_TASK_SUCCESS = 'GET_TASK_SUCCESS';


export function ActionFetchDaysRequest(request) {
  return {
    type: GET_DAYS_REQUEST,
    payload: request
  };
}

export function ActionFetchDaysSuccess(dayList) {
  return {
    type: GET_DAYS_SUCCESS,
    payload: dayList
  };
}



export function ActionFetchDayRequest(id) {
  return {
    type: GET_DAY_REQUEST,
    payload: id
  };
}

export function ActionFetchDaySuccess(day) {
  return {
    type: GET_DAY_SUCCESS,
    payload: day
  };
}



export function ActionCreateDayRequest(day) {
  return {
    type: CREATE_DAY_REQUEST,
    payload: day
  };
}

export function ActionCreateDaySuccess(day) {
  return {
    type: CREATE_DAY_SUCCESS,
    payload: day
  };
}



export function ActionUpdateDayRequest(request) {
  return {
    type: UPDATE_DAY_REQUEST,
    payload: request
  };
}

export function ActionUpdateDaySuccess(request) {
  return {
    type: UPDATE_DAY_SUCCESS,
    payload: request
  };
}



export function ActionDeleteDayRequest(id) {
  return {
    type: DELETE_DAY_REQUEST,
    payload: id
  };
}

export function ActionDeleteDaySuccess(id) {
  return {
    type: DELETE_DAY_SUCCESS,
    payload: id
  };
}


export function ActionSetTaskRequest(request) {
  return {
    type: GET_TASK_REQUEST,
    payload: request
  };
}

export function ActionSetTaskSuccess(request) {
  return {
    type: GET_TASK_SUCCESS,
    payload: request
  };
}
