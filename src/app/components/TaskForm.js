import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { compose, withState, withHandlers } from 'recompose';

const finalEnhance = compose(
    withState('name', 'changeName', ({ name }) => name),
    withHandlers({
        onChange: ({ changeName }) => (event) => {
            changeName(event.target.value)
        },
        onSubmit: ({ handleSubmit, name }) => (event) => {
            event.preventDefault();
            handleSubmit(name);
        }
    })
);
export const TaskForm = finalEnhance(({ name, onChange, onSubmit }) =>
    <form onSubmit={onSubmit}>
        <TextField value={name} onChange={onChange} name="name" />
        <br />
        <RaisedButton type="submit" label="Submit" primary={true} />
    </form>
)
