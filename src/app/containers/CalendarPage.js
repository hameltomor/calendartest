import React  from 'react';
import { connect } from "react-redux";
import '../assets/css/index.css';
import Calendar from 'rc-calendar/lib/Calendar';
import Select from 'rc-select';
import enUS from 'rc-calendar/lib/locale/en_US';
import moment from 'moment';
import 'moment/locale/en-gb';
const now = moment();
import { ActionFetchDaysRequest } from './../actions/days';
import DatePicker from 'material-ui/DatePicker';
import styled from 'styled-components';
import {
  compose,
  withHandlers,
  lifecycle,
  branch,
  renderComponent
} from 'recompose';


const DayTasks = styled.div`
  border-top: 1px solid #000;
  color: red;
`;

const mapStateToProps = (state) => {
  return {
    days: state.days.daysList
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDays: () => {
      dispatch(ActionFetchDaysRequest());
    }
  };
};

const loadingComponent = () => <div>Loading...</div>;

const finalEnhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withHandlers({
    getDayInfo: ({ days }) => (current) => {
      let day = moment(current).format('DD').toString();
      let currentData = moment(current).format("DDMMYYYY").toString();
      let count = 0;
      if (days[currentData]) {
        count = days[currentData].tasks ? days[currentData].tasks.length : 0;
      }
      return (
        <div className="rc-calendar-date">
          <div>
            {day}
          </div>
          <DayTasks>
            {count}
          </DayTasks>
        </div>);
    },
    onSelect: ({ history }) => (value) => {
      history
        .push('/day/' + value.format("DDMMYYYY"));
    }
  }),
  lifecycle({
    componentWillMount() {
      this.props.getDays();
    },
  }),
  branch(
    ({ days }) => !days,
    renderComponent(loadingComponent)
  )
);

const CalendarPage = finalEnhance(({ getDayInfo, onSelect }) =>
  <div style={{ zIndex: 1000, position: 'relative' }}>
    <Calendar
      style={{ margin: 10 }}
      Select={Select}
      defaultValue={now}
      dateRender={(current) => getDayInfo(current)}
      onSelect={onSelect}
      locale={enUS}
    />
  </div>
)

export default CalendarPage;