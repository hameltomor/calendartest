import React from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom'
import { ActionDeleteDayRequest, ActionUpdateDayRequest, ActionFetchDayRequest } from './../actions/days';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import { List, ListItem } from 'material-ui/List';
import { grey400 } from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
import {
  compose,
  withHandlers,
  lifecycle,
  branch,
  renderComponent
} from 'recompose';

const TasksWrapper = styled.div`
  width: 300px;
`;

const iconButtonElement = (
  <IconButton
    touch={true}
    tooltip="more"
    tooltipPosition="bottom-left"
  >
    <MoreVertIcon color={grey400} />
  </IconButton>
);

const mapStateToProps = (state, ownProps) => {
  let dayId = ownProps.match.params.dayId;
  return {
    day: state.days.selectedDay,
    dayId
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDay: (id) => {
      dispatch(ActionFetchDayRequest(id));
    },
    updateDay: (day) => {
      dispatch(ActionUpdateDayRequest(day));
    },
  };
};

const finalEnhance = compose(
  connect(mapStateToProps, mapDispatchToProps),
  withHandlers({
    deleteTask: ({ day, updateDay }) => (event, index) => {
      let newDay = Object.assign({}, day);
      newDay.tasks.splice(index, 1);
      updateDay(newDay);
    },
    addTask: ({ dayId }) => () => {
      return (
        <RaisedButton primary={true} >
          <Link to={`./${dayId}/new`}>Add task</Link>
        </RaisedButton>
      );
    }
  }),
  lifecycle({
    componentWillMount() {
      this.props.getDay(this.props.dayId);
    },
  }),
  branch(
    ({ day }) => !day,
    renderComponent(({ addTask }) => addTask())
  )
);

const DayList = finalEnhance(({ day, deleteTask, addTask }) =>
  <TasksWrapper>
    <List>
      <Subheader>Task list</Subheader>
      {
        day.tasks.map((task, index) => {
          return (
            <ListItem
              key={index}
              rightIconButton={
                <IconMenu iconButtonElement={iconButtonElement}>
                  <MenuItem>
                    <Link to={`./${day.id}/${index}`}>Edit</Link>
                  </MenuItem>
                  <MenuItem onClick={deleteTask}>Delete</MenuItem>
                </IconMenu>
              }
              primaryText={task}
            />
          )
        })
      }
    </List>
    {addTask()}
  </TasksWrapper>
);

export default DayList;
