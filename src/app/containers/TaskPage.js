import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from "react-redux";
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import { TaskForm } from './../components/TaskForm';
import { ActionCreateDayRequest, ActionUpdateDayRequest, ActionFetchDayRequest, ActionSetTaskRequest } from './../actions/days';
import {
    compose,
    withHandlers,
    lifecycle,
    branch,
    renderComponent
} from 'recompose';

const loadingComponent = () => <div>Loading...</div>;


const mapStateToProps = (state, ownProps) => {
    let taskId = ownProps.match.params.taskId;
    let dayId = ownProps.match.params.dayId;
    let isAdd = taskId === 'new';
    let task = isAdd ? '' : state.days.activeTask;
    return {
        day: state.days.selectedDay,
        task,
        taskId,
        isAdd,
        dayId
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getDay: (id) => {
            dispatch(ActionFetchDayRequest(id));
        },
        updateDay: (day) => {
            dispatch(ActionUpdateDayRequest(day));
        },
        setTask: (req) => {
            dispatch(ActionSetTaskRequest(req));
        }
    };
};

const finalEnhance = compose(
    connect(mapStateToProps, mapDispatchToProps),
    withHandlers({
        saveDay: ({ history, updateDay, day }) => () => {
            updateDay(day);
            history
                .push('/day/' + day.id);
        },
    }),
    withHandlers({
        handleSubmit: ({ day, taskId, dayId, isAdd, saveDay }) => (value) => {
            let newDay = null;
            if (day) {
                newDay = Object.assign({}, day);
                if (!isAdd) {
                    newDay.tasks[taskId] = value;
                }
                else {
                    day.tasks.push(value);
                }
            }
            else {
                newDay = { id: dayId, tasks: [value] };
            }
            saveDay(newDay);
        }
    }),
    lifecycle({
        componentWillMount() {
            this.props.setTask({ day: this.props.dayId, index: this.props.taskId });
        },
    }),
    branch(
        ({ task }) => task === null,
        renderComponent(loadingComponent)
    )
);

const TaskPage = finalEnhance(({ task, handleSubmit }) =>
    <TaskForm name={task} handleSubmit={handleSubmit} />
);

export default TaskPage;
