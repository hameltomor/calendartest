import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import CalendarPage from "./containers/CalendarPage";
import DayList from "./containers/DayList";
import TaskPage from "./containers/TaskPage";
import { Provider } from "react-redux";
import { store } from './reducers';
import createBrowserHistory from 'history/createBrowserHistory';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';


// Needed for onTouchTap 
// http://stackoverflow.com/a/34015469/988941 
injectTapEventPlugin();

const history = createBrowserHistory()

class App extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <Router history={history}>
                    <Switch>
                        <Route exact path='/' component={CalendarPage} />
                        <Route path='/day/:dayId/:taskId' component={TaskPage} />
                        <Route path='/day/:dayId' component={DayList} />
                    </Switch>
                </Router>
            </MuiThemeProvider>
        );
    }

}
render(
    <Provider store={store}>
        <App />
    </Provider>, window.document.getElementById("app"));
