import { createStore, combineReducers, applyMiddleware } from "redux";
import createSagaMiddleware from 'redux-saga';
import sagaDaysData from './../saga';
import {
    GET_DAYS_REQUEST,
    GET_DAYS_SUCCESS,

    GET_DAY_REQUEST,
    GET_DAY_SUCCESS,

    CREATE_DAY_REQUEST,
    CREATE_DAY_SUCCESS,

    UPDATE_DAY_REQUEST,
    UPDATE_DAY_SUCCESS,

    DELETE_DAY_REQUEST,
    DELETE_DAY_SUCCESS,

    GET_TASK_REQUEST,
    GET_TASK_SUCCESS
} from './../actions/days';

export const initialState = {
    loading: false,
    daysList: null,
    selectedDay: null,
    activeTask: null
};

export const daysReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DAYS_REQUEST:
            state = { ...state, daysList: null, loading: true };
            break;
        case GET_DAYS_SUCCESS:
            state = { ...state, daysList: action.payload, loading: false };
            break;


        case GET_DAY_REQUEST:
            state = { ...state, selectedDay: null, loading: true };
            break;
        case GET_DAY_SUCCESS:
            state = { ...state, selectedDay: action.payload, loading: false };
            break;


        case CREATE_DAY_REQUEST:
            state = { ...state, loading: true };
            break;
        case CREATE_DAY_SUCCESS:
            state = { ...state, daysList: [...state.daysList, action.payload], loading: false };
            break;


        case UPDATE_DAY_REQUEST:
            state = { ...state, loading: true };
            break;
        case UPDATE_DAY_SUCCESS:
            state = { ...state, loading: false };
            break;


        case DELETE_DAY_REQUEST:
            state = { ...state, daysList: [...state.daysList, action.payload], loading: true };
            break;
        case DELETE_DAY_SUCCESS:
            state.daysList.splice(action.payload, 1);
            state = { ...state, loading: false };
            break;

        case GET_TASK_REQUEST:
            state = { ...state, activeTask: null, loading: true };
            break;
        case GET_TASK_SUCCESS:
            let activeTask = action.payload.day ? action.payload.day.tasks[action.payload.index] : null;
            let selectedDay = action.payload.day ? action.payload.day : null;
            state = { ...state, selectedDay: action.payload.day, activeTask, loading: false };
            break;
    }
    return state;
};

export const sagaMiddleware = createSagaMiddleware()
export const store = createStore(combineReducers({ days: daysReducer }), applyMiddleware(sagaMiddleware));
sagaMiddleware.run(sagaDaysData);