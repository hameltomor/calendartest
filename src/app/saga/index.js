import { call, take, put, cps, takeLatest } from 'redux-saga/effects';

import {
    GET_DAYS_REQUEST,
    ActionFetchDaysSuccess,

    GET_DAY_REQUEST,
    ActionFetchDaySuccess,

    CREATE_DAY_REQUEST,
    ActionCreateDaySuccess,

    UPDATE_DAY_REQUEST,
    ActionUpdateDaySuccess,

    DELETE_DAY_REQUEST,
    ActionDeleteDaySuccess,

    GET_TASK_REQUEST,
    ActionSetTaskSuccess
} from './../actions/days';
import firebase from './../firebase.js';

let db = firebase.database();

export function* sagaGetDays(req) {
    let daysRef = db.ref('/days');
    const result = yield call([daysRef, daysRef.once], 'value');
    yield put(ActionFetchDaysSuccess(result.val()));
}

export function* sagaGetDay(req) {
    let daysRef = db.ref('/days/' + req.payload);
    let day = yield call([daysRef, daysRef.once], 'value');
    yield put(ActionFetchDaySuccess(day.val()));
}

export function* sagaCreateDay(req) {
    console.log('sagaCreateDay', req);
    // const product = req.product;
    // const result = yield call(api.createProduct, product);
    // yield put(ActionCreateDaySuccess(result.data));
}


export function* sagaUpdateDay(req) {
    let daysRef = db.ref('/days/' + req.payload.id);
    yield call([daysRef, daysRef.update], req.payload);
    yield put(ActionUpdateDaySuccess());
}

export function* sagaDeleteDay(req) {
    console.log('sagaDeleteDay', req);
    // const product = req.product;
    // const result = yield call(api.createProduct, product);
    // yield put(ActionDeleteDaySuccess(result.data));
}

export function* sagaGetTask(req) {
    let daysRef = db.ref('/days/' + req.payload.day);
    let day = yield call([daysRef, daysRef.once], 'value');
    yield put(ActionSetTaskSuccess({ day: day.val(), index: req.payload.index }));
}



export function* sagaDaysData() {
    yield takeLatest(GET_DAYS_REQUEST, sagaGetDays);
    yield takeLatest(GET_DAY_REQUEST, sagaGetDay);
    yield takeLatest(CREATE_DAY_REQUEST, sagaCreateDay);
    yield takeLatest(UPDATE_DAY_REQUEST, sagaUpdateDay);
    yield takeLatest(DELETE_DAY_REQUEST, sagaDeleteDay);
    yield takeLatest(GET_TASK_REQUEST, sagaGetTask);
}

export default sagaDaysData;
